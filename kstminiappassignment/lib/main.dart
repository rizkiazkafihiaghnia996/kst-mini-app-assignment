import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kstminiappassignment/app/core/values/colors.dart';
import 'package:kstminiappassignment/app/core/values/font.dart';
import 'package:kstminiappassignment/app/core/values/page_fade_transition.dart';
import 'package:kstminiappassignment/app/router/router_app.dart';
import 'package:kstminiappassignment/app/router/router_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  CustomTextStyle customTextStyle = CustomTextStyle();

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'KST Mini App Assignment',
      getPages: AppPages.pages,
      theme: ThemeData(
        pageTransitionsTheme: const PageTransitionsTheme(builders: {
            TargetPlatform.iOS: FadeTransitionBuilder(),
            TargetPlatform.android: FadeTransitionBuilder(),
          }),
          scaffoldBackgroundColor: Colors.white,
          splashFactory: InkRipple.splashFactory,
          primaryColor: contextYellow,
          appBarTheme: AppBarTheme(
            shadowColor: Colors.black26,
            color: Colors.white,
            centerTitle: true,
            iconTheme: const IconThemeData(color: Colors.black),
            titleTextStyle: customTextStyle.h5(),
          )
      ),
      initialRoute: HomeViewRoute,
    );
  }
}

