import 'package:get/get.dart';
import 'package:kstminiappassignment/app/modules/binding/main_binding.dart';
import 'package:kstminiappassignment/app/modules/view/home.dart';
import 'package:kstminiappassignment/app/modules/view/user_detail.dart';
import 'package:kstminiappassignment/app/modules/view/web_view.dart';
import 'package:kstminiappassignment/app/router/router_page.dart';

class AppPages {
  static final pages = [
    GetPage(
      name: HomeViewRoute, 
      page: () => HomeView(),
      binding: MainBinding()
    ),
    GetPage(
      name: UserDetailViewRoute, 
      page: () => UserDetailView(),
      binding: MainBinding()
    ),
    GetPage(
      name: WebViewRoute,
      page: () => const WebViewScreen()
    )
  ];
}