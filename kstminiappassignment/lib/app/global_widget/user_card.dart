import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kstminiappassignment/app/core/values/colors.dart';
import 'package:kstminiappassignment/app/core/values/font.dart';
import 'package:kstminiappassignment/app/router/router_page.dart';

class UserCard extends StatelessWidget {
  final String username;
  final String name;
  final double latitude;
  final double longitude;
  final int index;
  UserCard({ 
    Key? key, 
    required this.username, 
    required this.name, 
    required this.latitude, 
    required this.longitude, 
    required this.index }) : super(key: key);
  CustomTextStyle customTextStyle = CustomTextStyle();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.toNamed(UserDetailViewRoute, arguments: {
          'latitude': latitude,
          'longitude': longitude,
          'index': index
        });
      },
      child: Container(
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0, 3)
            )
          ]
        ),
        margin: const EdgeInsets.only(bottom: 8),
        height: Get.mediaQuery.size.height / 6,
        width: Get.mediaQuery.size.width,
        padding: const EdgeInsets.all(8),
        child: Row(
          children: [
            const Expanded(
              flex: 3,
              child: Icon(
                Icons.account_circle_rounded,
                color: contextYellow,
                size: 80,
              ),
            ),
            Expanded(
              flex: 4,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: Text(
                      username.toLowerCase(),
                      style: customTextStyle.h4(
                        color: contextYellow
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  const SizedBox(height: 8),
                  Flexible(
                    child: Text(
                      name,
                      style: customTextStyle.h5(
                        fontWeight: FontWeight.normal,
                        color: Colors.white
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}