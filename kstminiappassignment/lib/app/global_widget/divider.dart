import 'package:flutter/material.dart';
import 'package:kstminiappassignment/app/core/values/colors.dart';

class CustomVerticalDivider extends StatelessWidget {
  double height;
  double width;
  CustomVerticalDivider({ Key? key, this.height = 20, this.width = 2}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 8, right: 8),
      child: SizedBox(
        height: height,
        width: width,
        child: Container(
          color: contextYellow,
        ),
      ),
    );
  }
}

class CustomHorizontalDivider extends StatelessWidget {
  double height;
  double width;
  CustomHorizontalDivider({ Key? key, this.height = 2, required this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 8, right: 8),
      child: SizedBox(
        height: height,
        width: width,
        child: Container(
          color: contextYellow,
        ),
      ),
    );
  }
}