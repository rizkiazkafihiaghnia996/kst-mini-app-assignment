import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:kstminiappassignment/app/core/values/colors.dart';
import 'package:kstminiappassignment/app/core/values/font.dart';

class CopyableText extends StatefulWidget {
  final String snackbarMessage;
  final String content;
  const CopyableText({ Key? key, 
    required this.snackbarMessage, 
    required this.content }) : super(key: key);

  @override
  State<CopyableText> createState() => _CopyableTextState();
}

class _CopyableTextState extends State<CopyableText> {
  CustomTextStyle customTextStyle = CustomTextStyle();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onLongPress: () {
        Clipboard.setData(
                ClipboardData(text: widget.content))
            .then((value) => Get.showSnackbar(GetSnackBar(
                  margin: const EdgeInsets.all(16),
                  message: widget.snackbarMessage,
                  messageText: Text(
                    widget.snackbarMessage,
                    style: customTextStyle.h5(color: textBlack, fontWeight: FontWeight.normal),
                  ),
                  backgroundColor: Colors.white,
                  borderRadius: 8,
                  duration: const Duration(milliseconds: 1000),
                  boxShadows: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.1),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: const Offset(0, 3))
                  ],
                )));
      },
      child: Text(
        widget.content,
        style: customTextStyle.h5(color: Colors.white),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
}