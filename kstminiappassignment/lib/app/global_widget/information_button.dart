import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kstminiappassignment/app/core/values/colors.dart';
import 'package:kstminiappassignment/app/core/values/font.dart';


class InformationButton extends StatelessWidget {
  Function()? onTap;
  final String content;
  final IconData icon;
  final bool isNotAButton;
  InformationButton({ Key? key, 
     this.onTap, 
     required this.content,
     required this.icon, 
     required this.isNotAButton 
  }) : super(key: key);

  CustomTextStyle customTextStyle = CustomTextStyle();
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: Get.mediaQuery.size.height / 8,
        width: Get.mediaQuery.size.width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16), color: contextYellow),
        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          Expanded(
            flex: 2,
            child: Icon(
              icon,
              size: 40,
              color: Colors.black,
            ),
          ),
          Expanded(
            flex: 6,
            child: Text(
              content,
              style: customTextStyle.h5(),
            ),
          ),
          isNotAButton == false ? const Expanded(
            flex: 2,
            child: Icon(
              Icons.arrow_right_outlined,
              size: 60,
            ),
          ) : const Expanded(
            flex: 2,
            child: SizedBox.shrink()
          )
        ]),
      ),
    );
  }
}