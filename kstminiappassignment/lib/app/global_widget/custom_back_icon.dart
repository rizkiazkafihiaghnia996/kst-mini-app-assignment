import 'package:flutter/material.dart';
import 'package:kstminiappassignment/app/core/values/colors.dart';

class CustomBackIcon extends StatelessWidget {
  bool isOutline;
  CustomBackIcon({this.isOutline = false, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: isOutline ? Colors.transparent : Colors.black,
            border: Border.all(color: Colors.black, width: 1)),
        child: IconButton(
          icon: Icon(
            Icons.arrow_left,
            color: isOutline ? dividerColorPrimary : contextYellow,
            size: 35,
          ),
          onPressed: () {
            Navigator.maybePop(context);
          },
        ),
      ),
    );
  }
}