import 'package:kstminiappassignment/app/data/api/api_provider.dart';
import 'package:kstminiappassignment/app/data/model/users_response.dart';

class Repository {
  ApiProvider apiProvider = ApiProvider();

  Future<List<UsersResponse>> getUsersData() async {
    final response = await apiProvider.getUsersData();
    return (response as List).map((e) => 
        UsersResponse.fromJson(e))
        .toList();
  }
}