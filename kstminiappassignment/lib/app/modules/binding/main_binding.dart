import 'package:get/get.dart';
import 'package:kstminiappassignment/app/modules/controller/main_controller.dart';

class MainBinding implements Bindings {

  @override
  void dependencies() {
    Get.lazyPut<MainController>(() => MainController());
  }
}