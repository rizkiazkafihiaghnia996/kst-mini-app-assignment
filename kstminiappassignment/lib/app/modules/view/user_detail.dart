// ignore_for_file: sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kstminiappassignment/app/core/utils/captalize_first_letter.dart';
import 'package:kstminiappassignment/app/core/values/colors.dart';
import 'package:kstminiappassignment/app/core/values/font.dart';
import 'package:kstminiappassignment/app/global_widget/copyable_text.dart';
import 'package:kstminiappassignment/app/global_widget/custom_back_icon.dart';
import 'package:kstminiappassignment/app/global_widget/divider.dart';
import 'package:kstminiappassignment/app/global_widget/information_button.dart';
import 'package:kstminiappassignment/app/global_widget/skeleton_loader.dart';
import 'package:kstminiappassignment/app/modules/controller/main_controller.dart';
import 'package:kstminiappassignment/app/router/router_page.dart';

class UserDetailView extends GetView<MainController> {

  @override
  Widget build(BuildContext context) {
    CustomTextStyle customTextStyle = CustomTextStyle();
    MainController controller = Get.find<MainController>();
    int index = Get.arguments['index'];
    double lat = Get.arguments['latitude'];
    double lng = Get.arguments['longitude'];

    return Scaffold(
     backgroundColor: contextYellow,
     body: SafeArea(
       child: Container(
          height: Get.mediaQuery.size.height,
          child: Stack(
            children: [
              Positioned(child: CustomBackIcon(), top: 0, left: 10),
              Container(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: Get.mediaQuery.size.height / 1.4,
                  width: Get.mediaQuery.size.width,
                  padding: const EdgeInsets.fromLTRB(16, 24, 16, 24),
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(32),
                          topRight: Radius.circular(32)),
                      color: Colors.black),
                  child: SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: Obx(() => controller.usersLoading == true ? Column(
                            children: List.generate(3, (index) => 
                              const SkeletonLoader())) : 
                    Column(
                      children: [
                        const SizedBox(height: 100),
                        Text(
                          controller.usersList[index].username.toLowerCase(),
                          style: customTextStyle.h1(color: contextYellow),
                        ),
                        Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  controller.usersList[index].name,
                                  style: customTextStyle.h5(color: Colors.white),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                CustomVerticalDivider(),
                                Flexible(
                                  child: CopyableText(
                                    snackbarMessage: 'Copied to clipboard', 
                                    content: controller.usersList[index].email
                                  )
                                ),
                              ],
                            ),
                            const SizedBox(height: 8),
                            CopyableText(
                              snackbarMessage: 'Copied to clipboard', 
                              content: controller.usersList[index].phone.contains(' ') == true 
                                  ? controller.usersList[index].phone
                                  .substring(0, controller.usersList[index].phone
                                  .indexOf(' ')) : controller.usersList[index].phone,
                            )
                          ],
                        ),
                        const SizedBox(height: 8),
                        CustomHorizontalDivider(
                          width: Get.mediaQuery.size.width
                        ),
                        const SizedBox(height: 16),
                        InformationButton(
                          isNotAButton: false,
                          onTap: () {
                            controller.launchGmaps(lat, lng);
                          }, 
                          content: controller.usersList[index].address.street
                                        + ', ' + controller.usersList[index].address.suite
                                        + ', ' + controller.usersList[index].address.city
                                        + ', ' + controller.usersList[index].address.zipcode, 
                          icon: Icons.location_pin
                        ),
                        const SizedBox(height: 8),
                        InformationButton(
                          isNotAButton: true,
                          content: '${controller.usersList[index].company.name}\n${controller.usersList[index].company.bs.capitalizeFirstLetter()}',
                          icon: Icons.business
                        ),
                        const SizedBox(height: 8),
                        InformationButton(
                          isNotAButton: false,
                          onTap: () {
                            Get.toNamed(WebViewRoute, arguments: {
                              'url': controller.usersList[index].website
                            });
                          }, 
                          content: controller.usersList[index].website, 
                          icon: Icons.link
                        ),
                      ],
                    )),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                height: Get.mediaQuery.size.height / 2,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(100)
                  ),
                  child: const Icon(
                    Icons.account_circle_rounded,
                    size: 200,
                    color: contextYellow,
                  ),
                ),
              )
            ],
          ),
       ),
     ),
    );
  }
}