import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kstminiappassignment/app/core/values/colors.dart';
import 'package:kstminiappassignment/app/core/values/font.dart';
import 'package:kstminiappassignment/app/global_widget/skeleton_loader.dart';
import 'package:kstminiappassignment/app/global_widget/user_card.dart';
import 'package:kstminiappassignment/app/modules/controller/main_controller.dart';

class HomeView extends GetView<MainController> {
  @override
  Widget build(BuildContext context) {
    MainController controller = Get.find<MainController>();
    CustomTextStyle customTextStyle = CustomTextStyle();

    return Scaffold(
      backgroundColor: contextYellow,
      body: Obx(
        () {
          if (controller.usersLoading == true || controller.usersList == null) {
            return SafeArea(
              child: Column(
                children: List.generate(5, (index) => const SkeletonLoader()),
              ),
            );
          } else {
            return SafeArea(
              child: Container(
                height: Get.mediaQuery.size.height,
                width: Get.mediaQuery.size.width,
                padding: const EdgeInsets.all(8),
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Friends',
                        style: customTextStyle.h1(),
                      ),
                      const SizedBox(height: 4),
                      Text(
                        'Yay!, you\'ve got ${controller.usersList.length.toString()} added friends.',
                        style: customTextStyle.h5(fontWeight: FontWeight.normal),
                      ),
                      const SizedBox(height: 16),
                      Builder(
                        builder: (context) {
                          return Column(
                            children: List.generate(
                              controller.usersList.length, (index) => UserCard(
                                username: controller.usersList[index].username, 
                                name: controller.usersList[index].name,
                                latitude: double.parse(
                                    controller.usersList[index]
                                        .address
                                        .geo
                                        .lat
                                ),
                                longitude: double.parse(
                                    controller.usersList[index]
                                        .address
                                        .geo
                                        .lng
                                ),
                                index: index,
                              )
                            ),
                          );
                        }
                      ),
                    ],
                  ),
                ),
              ),
            );
          }
        }
      ),
    );
  }
}