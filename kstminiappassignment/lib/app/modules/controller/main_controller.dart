import 'package:get/get.dart';
import 'package:kstminiappassignment/app/data/api/repository.dart';
import 'package:kstminiappassignment/app/data/model/users_response.dart';
import 'package:kstminiappassignment/app/global_widget/error_snackbar.dart';
import 'package:url_launcher/url_launcher.dart';

class MainController extends GetxController {
  Repository repository = Repository();

  @override
  void onInit() {
    getUsersData();
    super.onInit();
  }

  RxBool _usersLoading = false.obs;
  RxList<UsersResponse> _usersList = <UsersResponse>[].obs;

  bool get usersLoading => _usersLoading.value;
  List<UsersResponse> get usersList => _usersList;

  set usersLoading(bool usersLoading) =>
      this._usersLoading.value = usersLoading;
  set usersList(List<UsersResponse> usersList) =>
      this._usersList.value = usersList;

  Future<List<UsersResponse>> getUsersData() async {
    usersLoading = true;
    List<UsersResponse> res = await repository.getUsersData();
    usersList = res;
    usersLoading = false;
    return usersList;
  }

  /*launchGmaps(double lat, double lng) async {
    Uri androidMapsUrl =
        Uri.parse('https://www.google.com/maps/search/?api=1&map_action=map&query=$lat,$lng&zoom=100');
    Uri appleMapsUrl = Uri.parse('https://maps.apple.com/?sll=$lat,$lng');

    if (await canLaunchUrl(androidMapsUrl)) {
      await launchUrl(androidMapsUrl);
    } else if (await canLaunchUrl(appleMapsUrl)) {
      await launchUrl(appleMapsUrl);
    } else {
      errorSnackbar('Something went wrong', 'Google Maps failed to launch');
    }
  }*/

  launchGmaps(double lat, double lng) async {
    String androidMapsUrl =
      'https://www.google.com/maps/search/?api=1&map_action=map&query=$lat,$lng&zoom=100';
    String appleMapsUrl = 'https://maps.apple.com/?sll=$lat,$lng';

    if (await canLaunch(androidMapsUrl)) {
      await launch(androidMapsUrl);
    } else if (await canLaunch(appleMapsUrl)) {
      await launch(appleMapsUrl);
    } else {
      errorSnackbar('Something went wrong', 'Google Maps failed to launch');
    }
  }
}